Target Setup and Testing
========================

Setup the Target:
^^^^^^^^^^^^^^^^^

1- ssh to your target or run the following commands on your target directly.

2- make sure libffi is included with your image otherwise copy it over from your sdp

.. code-block:: bash

    scp ~/qnx710/target/qnx7/x86_64/usr/lib/libffi.so.6 root@<target_ip>:/usr/lib/
    ln -s /usr/lib/libffi.so.6 /usr/lib/libffi.so

3- Download CA certificates bundle on your PC then copy it over to your target:

On your target:
    
.. code-block:: bash
    
    mkdir -p /etc/curl

On your PC:

.. code-block:: bash

    curl --time-cond cacert.pem https://curl.se/ca/cacert.pem
    scp cacert.pem root@<target_ip_address>:/etc/curl/
    
4- Add the following line to end of your /etc/profile on target and restart or logout and back in for the change to take effect.

.. code-block:: bash

    export CURL_CA_BUNDLE=/etc/curl/cacert.pem

5- If a /tmp directory does not exist, add one on your target. Please note that this will require having a writable / partition, otherwise you can create another partition and mount it on top of / or /tmp

.. code-block:: bash

    mkdir /tmp

6- Update system time with ntpdate (on target). **Please use the appropriate time server for your region.** The following time server is for Canada, but others can be found at https://www.ntppool.org/zone.

.. code-block:: bash

    ntpdate 0.ca.pool.ntp.org

7- Install pip on your target

.. code-block:: bash

    curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
    python3 get-pip.py

8- Install python dependencies on your target.

.. code-block:: bash

    pip install -U \
    colcon-common-extensions \
    importlib-metadata \
    importlib-resources \
    lark-parser

9- create a directory for ROS2's installation.

.. code-block:: bash

    mkdir -p /opt/ros/rolling

10- Get the ip address of your target

.. code-block:: bash

    ifconfig

11- Check the amount of space available on your target and make sure you have enough space to copy the files over.

.. code-block:: bash

    df -h

12- Copy ROS 2 to your target.

Note: you will have to replace "your_target_architecture" with your target architecture (e.g: "aarch64le" or "x86_64).

On host:

.. code-block:: bash

    cd ~/ros2_rolling/install/<your_target_arch>/
    tar -czvf ros2_rolling.tar.gz *
    scp ros2_rolling.tar.gz root@<target_ip_address>:/opt/ros/rolling/

On target:

.. code-block:: bash

    cd /opt/ros/rolling
    tar -xzvf ros2_rolling.tar.gz

All the necessary files to run ROS 2 are now on your target.

13- Add the following lines to the end of your /etc/profile file

.. code-block:: bash

    export COLCON_CURRENT_PREFIX=/opt/ros/rolling
    export PYTHONPATH=/opt/ros/rolling/usr/lib/python3.8/site-packages
    . /opt/ros/rolling/local_setup.sh
    
14- Logout and login or reboot. On QNX, a reboot can be done using `shutdown`.

Test the installation
^^^^^^^^^^^^^^^^^^^^^

1- ssh to your target and on one terminal run the following.

.. code-block:: bash

    ros2 run demo_nodes_cpp talker

2- On another terminal run the following.

.. code-block:: bash

    ros2 run demo_nodes_py listener

You should see the demos running on both terminals if the installation went successful.
